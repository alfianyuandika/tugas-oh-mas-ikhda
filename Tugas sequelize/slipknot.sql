CREATE DATABASE slipknot

use slipknot

CREATE TABLE personil (
	id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	nama VARCHAR(255) NOT NULL
	);

CREATE TABLE tools (
     id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
     nama VARCHAR(255) NOT NULL,
     id_personil BIGINT UNSIGNED NOT NULL,
     FOREIGN KEY (id_personil) REFERENCES personil(id)
     );

Insert into personil(nama) values
("Corey Taylor"),
("Chris Fehn"),
("James Root");

insert into tools(nama, id_personil) values
("Singer", 1),
("Percussionist", 2),
("Guitar", 3);